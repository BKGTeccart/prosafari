﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Safari
{
    public partial class getAboutElements : System.Web.UI.Page
    {

        private PageAbout about = new PageAbout
        {
            titre= "nd",
            desc_about= "nd",
            titreBas = "nd",
            sousTitreGauche = "nd",
            sousTitreCentre = "nd",
            sousTitreDroit = "nd",
            descGauche = "nd",
            descCentre = "nd",
            descDroit = "nd"
            
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\ASP\Laboratoire 1 - Safari\Safari\Safari\App_Data\Safari.mdf;Integrated Security=True");
            getElements_about(myCon);

        }

        public void getElements_about(SqlConnection laConnexion)
        {
            try
            {

                laConnexion.Open();
                SqlCommand commande = new SqlCommand("select * from About;", laConnexion);
                SqlDataReader reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    about = new PageAbout
                    {
                        titre = reader["titre"].ToString(),
                        desc_about = reader["desc_about"].ToString(),
                        titreBas = reader["titreBas"].ToString(),
                        sousTitreGauche = reader["sousTitreGauche"].ToString(),
                        sousTitreCentre = reader["sousTitreCentre"].ToString(),
                        sousTitreDroit = reader["sousTitreDroit"].ToString(),
                        descGauche = reader["descGauche"].ToString(),
                        descCentre = reader["descCentre"].ToString(),
                        descDroit = reader["descDroit"].ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                Response.Write("error:" + ex.Message);
            }
            finally
            {

                var json = new JavaScriptSerializer().Serialize(about);  //prend un objet js et retourne un objet json -> récupérer dans data

                Response.Write(json);

                laConnexion.Close();
            }
        }
    }
    
}