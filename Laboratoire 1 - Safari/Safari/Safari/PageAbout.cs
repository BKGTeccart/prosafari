﻿namespace Safari
{
    public class PageAbout
    {
        public string titre { get; set; }
        public string desc_about { get; set; }
        public string titreBas { get; set; }
        public string sousTitreGauche { get; set; }
        public string sousTitreCentre { get; set; }
        public string sousTitreDroit { get; set; }
        public string descGauche { get; set; }
        public string descCentre { get; set; }
        public string descDroit { get; set; }
    }
}