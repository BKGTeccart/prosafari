﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Safari
{
    public partial class insertData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RECUPERER
            string leNom = Request["nom_"].ToString();
            string lEmail = Request["email_"].ToString();
            string leCell = Request["cell_"].ToString();
            string leMessage = Request["message_"].ToString();

            insertData_(leNom, lEmail, leCell, leMessage);
        }


        public void insertData_(string cnom, string cemail, string ccell, string cmessage)
        {
            //instanciation du linqcontact
            linqContactbdDataContext db_c = new linqContactbdDataContext();
            //instanciation de la table contact avec les parametres d'ajout
            Contact cn = new Contact { nom = cnom, email = cemail, cell = ccell, message = cmessage };

            //executer la commande
            db_c.Contact.InsertOnSubmit(cn);
            //commit
            db_c.SubmitChanges();
        }


    }
}