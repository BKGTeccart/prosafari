﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="Safari.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="lightbox.min.css" rel="stylesheet" />
    <script src="lightbox-plus-jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="gal">
        <div class="galery">
            <a href="Images/laGallerie/akshour.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/akshour.jpg" />
            </a>
            <a href="Images/laGallerie/casablanca-morocco--mosque-hassan-ii-arcade-gallery.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/casablanca-morocco--mosque-hassan-ii-arcade-gallery.jpg" />

            </a>
            <a href="Images/laGallerie/Casablanca.jpg" data-lightbox="mygallery">

                <img src="Images/laGallerie/Casablanca.jpg" />

            </a>
            <a href="Images/laGallerie/HASSAN-TOWER-1024x768.jpg" data-lightbox="mygallery">

                <img src="Images/laGallerie/HASSAN-TOWER-1024x768.jpg" />

            </a>

            <a href="Images/laGallerie/hoceima.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/hoceima.jpg" />

            </a>
            <a href="Images/laGallerie/ouzoudfalls.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/ouzoudfalls.jpg" />

            </a>
            <a href="Images/laGallerie/riadFez.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/riadFez.jpg" />
            </a>

            <a href="Images/laGallerie/tours-in-camellos-marruecos.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/tours-in-camellos-marruecos.jpg" />
            </a>

            <a href="Images/laGallerie/merzouga.jpg" data-lightbox="mygallery">
                <img src="Images/laGallerie/merzouga.jpg" />
            </a>

            <a href="Images/walili.jpg" data-lightbox="mygallery">
                <img src="Images/walili.jpg" />
            </a>

            <a href="Images/marakesh.jpg" data-lightbox="mygallery">
                <img src="Images/marakesh.jpg" />
            </a>

            <a href="Images/imageExplore.jpg" data-lightbox="mygallery">
                <img src="Images/imageExplore.jpg" />
            </a>
        </div>
    </section>
    <script type="text/javascript" src="../javascript/lightbox-plus-jquery.min.js"></script>
</asp:Content>
