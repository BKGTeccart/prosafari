﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Safari.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="slideImage.js"></script>
    <script src="getBanners.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <!--<img id="Explore" src="Images/imageExplore.jpg" />-->
        <div class="container">

            <div class="slide_img" id="one">
            </div>

            <div class="slide_img" id="two">

            </div>

            <div class="slide_img" >
                <img id="three" src="Images/ExploreMorocco.jpg" />
            </div>

            <div class="nav">
                <label class="dots"  onclick="firstPicture()"></label>
                <label class="dots"  onclick="secondPicture()"></label>
                <label class="dots"  onclick="thirdPicture()"></label>
            </div>

        </div>

        <div class="leftDivision">
            <div class="positionnement" id="titreBanniereGauche"></div>
            <div>
                <img class="banieres" id="imageGauche" src="smtg" />
            </div>
            <h5 class="titreDesc" id="sousTitreBanniereGauche"></h5>
            <div class="description" id="descriptionGauche">
                
            </div>
            <button class="lebtnMore">More</button>
        </div>
        <div class="centerDivision">
            <div class="positionnement" id="titreBanniereCentre"></div>
            <div>
                <img class="banieres" id="imageCentre" src="smtg" />
            </div>
            <h5 class="titreDesc" id="sousTitreBanniereCentre"></h5>
            <div class="description" id="descriptionCentre">  
                
            </div>
            <button class="lebtnMore">More</button>
            
        </div>
        <div class="rightDivision">
            <div class="positionnement" id="titreBanniereDroite"></div>
            <div>
                <img class="banieres" id="imageDroite" src="smtg" />
            </div>
            <h5 class="titreDesc" id="sousTitreBanniereDroite"></h5>
            <div class="description" id="descriptionDroite">
            </div>
            <button class="lebtnMore">More</button>
        </div>
    </section>
</asp:Content>
