﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Safari.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="getAboutElements.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="headingDivision">
            <h2 id="aboutUs"></h2>
            <hr />
            <div class="textAbout" id="descAbout"></div>
            <button class="btnMoreAbout">More</button>
        </div>

        <h2 id="titreWhyC"></h2>

        <div class="leftDivision">
            <div class="positionnement" id="titreAboutGauche"></div>
            <div class="description" id="descAboutGauche"></div>
            <button class="lebtnMore">More</button>
        </div>
        <div class="centerDivision">
            <div class="positionnement" id="titreAboutCentre"></div>
            <div class="description" id="descAboutCentre"></div>
            <button class="lebtnMore">More</button>
        </div>
        <div class="rightDivision">
            <div class="positionnement" id="titreAboutDroit"></div>
            <div class="description" id="descAboutDroit"></div>
            <button class="lebtnMore">More</button>
        </div>

    </section>
</asp:Content>
