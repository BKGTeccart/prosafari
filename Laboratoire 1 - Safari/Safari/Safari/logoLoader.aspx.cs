﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Safari
{
    public partial class logoLoader : System.Web.UI.Page
    {
        /*
        private ImagesLogos i = new ImagesLogos
        {
            source_image = ""
        };
        */

        protected void Page_Load(object sender, EventArgs e)
        {
            
            SqlConnection myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\ASP\Laboratoire 1 - Safari\Safari\Safari\App_Data\Safari.mdf;Integrated Security=True");
            getLogos(myCon);

        }


        public void getLogos(SqlConnection laConn)
        {
            string lg = "";
            try
            {
                laConn.Open();
                SqlCommand commande = new SqlCommand("select * from Caroussel;", laConn);
                SqlDataReader reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    lg += reader["source_image"].ToString() + ";";
                   
                }
            }
            catch (Exception ex)
            {
                Response.Write("error:" + ex.Message);
            }
            finally
            {

                Response.Write(lg);

                laConn.Close();
            }
        }
    }
}