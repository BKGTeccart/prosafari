﻿$(document).ready(function () {
    $.get("BannersLoader.aspx", function (data) {
        var banners = data.split(";");

        $("#titreBanniereGauche").text(banners[0]);
        $("#imageGauche").attr('src', banners[1]);
        $("#sousTitreBanniereGauche").text(banners[2]);
        $("#descriptionGauche").text(banners[3]);
        $("#titreBanniereCentre").text(banners[4]);
        $("#imageCentre").attr('src', banners[5]);
        $("#sousTitreBanniereCentre").text(banners[6]);
        $("#descriptionCentre").text(banners[7]);
        $("#titreBanniereDroite").text(banners[8]);
        $("#imageDroite").attr('src', banners[9]);
        $("#sousTitreBanniereDroite").text(banners[10]);
        $("#descriptionDroite").text(banners[11]);
    })

});