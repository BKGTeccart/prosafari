﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace Safari
{
    public partial class dataLoader : System.Web.UI.Page
    {
        private PageAcceuilModel p = new PageAcceuilModel
        {
            champs1 = "Acceuil",
            champs2 = "A propos",
            champs3="Pages",
            champs4 = "Images",
            champs5 ="Blog",
            champs6 = "Contacts",
            titre="Safari"
        };

       

    protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\ASP\Laboratoire 1 - Safari\Safari\Safari\App_Data\Safari.mdf;Integrated Security=True");

            getMenu(myCon);

        }

        public void getMenu(SqlConnection Conn)
        {
            try
            {

            
            Conn.Open();
            SqlCommand commande = new SqlCommand("select * from Menu;", Conn);
            SqlDataReader reader = commande.ExecuteReader();

                while(reader.Read())
                {
                    p = new PageAcceuilModel
                    {
                        champs1 = reader["champs1"].ToString(),
                        champs2 = reader["champs2"].ToString(),
                        champs3 = reader["champs3"].ToString(),
                        champs4 = reader["champs4"].ToString(),
                        champs5 = reader["champs5"].ToString(),
                        champs6 = reader["champs6"].ToString(),
                        titre = reader["titre"].ToString()

                    };
                }
            }
            catch (Exception ex)
            {  
                Response.Write("error:" + ex.Message);
            }
            finally
            {

                var json = new JavaScriptSerializer().Serialize(p);  //prend un objet js et retourne un objet json -> récupérer dans data
                   
                    Response.Write(json);
                
                Conn.Close();
            }
        }

       

    }
}