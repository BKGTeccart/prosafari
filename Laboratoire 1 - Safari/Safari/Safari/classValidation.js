﻿//la classe de validation JS
/*
function Valider_contact() {
    var name = document.getElementById('txtName');
    var email = document.getElementById('txtEmail');
    var phone = document.getElementById('txtPhone');
    var message = document.getElementById('txtMessage');

    
    //cree un nouveau objet adherant:
    var adh = new adherant(name.value, email.value, phone.value, message.value);

    //alert(adh.getMessage());

    if (!adh.ValiderNom()) {
        name.style.backgroundColor = "red";
    }
    else {
        name.style.backgroundColor = "white";
    }

    if (!adh.ValiderEmail()) {
        email.style.backgroundColor = "red";
    }
    else {
        email.style.backgroundColor = "white";
    }

    if (!adh.ValiderPhone()) {
        phone.style.backgroundColor = "red";
    }
    else {
        phone.style.backgroundColor = "white";
    }

    if (!adh.ValiderMessage()) {
        message.style.backgroundColor = "red";
    }
    else {
        message.style.backgroundColor = "white";
    }
}

//cree le constructeur de l'objet adherant:
function adherant(nom, lemail, cell, message) {
    this.name = nom;
    this.email = lemail;
    this.phone = cell;
    this.message = message;
}

//getters and setters

adherant.prototype.getName = function () {
    return this.name;
}

adherant.prototype.getEmail = function () {
    return this.email;
}

adherant.prototype.getPhone = function () {
    return this.phone;
}

adherant.prototype.getMessage = function () {
    return this.message;
}

//--------------------------------------------------------

adherant.prototype.setName = function (nom) {
    return this.name = nom;
}

adherant.prototype.setEmail = function (mail) {
    return this.email = mail;
}

adherant.prototype.setPhone = function (cell) {
    return this.phone = cell;
}

adherant.prototype.setMessage = function (mess) {
    return this.message = mess;
}

//fonctions de validations :

//****NOM:
adherant.prototype.ValiderNom = function () {
    var status;
    for (var i = 0; i < this.name.length; i++) {
        if (((this.nom.charCodeAt(i) >= "A".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "Z".charCodeAt(0))) ||
            ((this.nom.charCodeAt(i) >= "a".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "z".charCodeAt(0)))) {
            status = true;

        }
        else {
            status = false;
        }
    }

    return status;
}

//****EMAIL:
adherant.prototype.ValiderEmail = function () {
    var emailRegEx = /[a-zA-Z0-9]+(\.|_|-)?[a-zA-Z0-9]+@teccart\.qc\.ca/;
    if (emailRegEx.test(this.email)) {
        return true;  //code
    }
    else {
        return false;
    }
}

//****PHONE:
adherant.prototype.ValiderPhone = function () {

    var telRegEx = /^(\(\d{3}\)) ?\d{3}-\d{4}/;
    if (telRegEx.test(this.phone)) {
        return true;  //code
    }
    else {
        return false;
    }


}

//****MESSAGE:
adherant.prototype.ValiderMessage = function () {

    if (isEmpty(adh.getMessage())) {
        return true;  //code
    }
    else {
        return false;
    }

}

*/

function sendInfos() {
    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;
    var phone = document.getElementById('txtPhone').value;
    var message = document.getElementById('txtMessage').value;

    var req = new XMLHttpRequest();
    req.open("GET", "insertData.aspx?nom_=" + name + "&email_=" + email + "&cell_=" + phone + "&message_=" + message, false);
    req.onload = function () {
        var reponse = req.responseText;
        $("#txtName").val("");
        $("#txtEmail").val("");
        $("#txtPhone").val("");
        $("#txtMessage").val("");
        alert("succees");
    }
    req.send(null);
}
