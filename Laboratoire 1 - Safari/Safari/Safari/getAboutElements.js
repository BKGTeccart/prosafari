﻿//ajax :
$(document).ready(function () {
    $.get("getAboutElements.aspx", function (data) {
        //var infos_menu = data.split("/");
        var infos_about = JSON.parse(data);
        //tous les champs de la base de donnees

        $("#aboutUs").text(infos_about.titre);
        $("#descAbout").text(infos_about.desc_about);
        $("#titreWhyC").text(infos_about.titreBas);
        $("#titreAboutGauche").text(infos_about.sousTitreGauche);
        $("#titreAboutCentre").text(infos_about.sousTitreCentre);
        $("#titreAboutDroit").text(infos_about.sousTitreDroit);
        $("#descAboutGauche").text(infos_about.descGauche);
        $("#descAboutCentre").text(infos_about.descCentre);
        $("#descAboutDroit").text(infos_about.descDroit);

    });
}


);