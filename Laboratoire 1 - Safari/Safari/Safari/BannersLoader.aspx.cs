﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Safari
{
    public partial class BannersLoader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\ASP\Laboratoire 1 - Safari\Safari\Safari\App_Data\Safari.mdf;Integrated Security=True");
            getBanners(myCon);
        }

        public void getBanners(SqlConnection myCon)
        {
            string myBanners = "";

            try
            {
                myCon.Open();
                SqlCommand commande = new SqlCommand("select * from Bannieres;", myCon);
                SqlDataReader reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    myBanners += reader["titre"].ToString() + ";";
                    myBanners += reader["images_src"].ToString() + ";";
                    myBanners += reader["sous_titre"].ToString() + ";";
                    myBanners += reader["description"].ToString() + ";";
                }
            }         
              catch (Exception ex)
            {
                Response.Write("error:" + ex.Message);
            }
            finally
            {
                //ENVOYER
                Response.Write(myBanners);
                myCon.Close();
            }

        }
    }
}