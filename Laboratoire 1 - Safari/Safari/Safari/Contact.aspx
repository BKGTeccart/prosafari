﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Safari.contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="classValidation.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="sectionContact">
        <div class="laGauche">
            <h5 class="lesSousTitres">ADRESS:</h5>
            <img src="Images/icons8-hangar-16.png" />
            <span>138 Atlantis Ln Kingsport Illinois 121164</span>

            <h5 class="lesSousTitres">PHONES:</h5>

            <img src="Images/icons8-phone-filled-16.png" />
            <span>800-2435-6785</span>
            <br />
            
            <img src="Images/icons8-office-phone-16.png" />
            <span>800-900-4040</span>

            <h5 class="lesSousTitres">E-MAIL:</h5>
            <img src="Images/icons8-new-post-filled-16.png" />
            <span class="colorationOrange">mail@demolink.org</span>
            <br />
            <br />
            
            <span>Download informations as: <span class="colorationOrange">vCard</span></span>
        </div>


        <div class="laDroite">
            <h5 class="lesSousTitres">MISCELLANEOUS INFORMATION: </h5>
            <span>Email us with any questions or inqueries or use our contact data.</span>
            <br />
            <br />
            <br />
            <div class="controls">
                <input class="lesTextbox" id="txtName" type="text" placeholder="Name" />
                <input class="lesTextbox" id="txtEmail" type="text" placeholder="Email" />
                <input class="lesTextbox" id="txtPhone" type="text" placeholder="Phone" />

            </div>
            <br />
            <textarea id="txtMessage" cols="55" rows="6" placeholder="Message"></textarea>
            <br />
            <br />
            <br />
            <input type="submit" id="send" value="Send" name="Send" onclick="sendInfos()" />
            <input type="submit" id="clear" value="Clear" name="Clear" />

        </div>
    </section>
</asp:Content>
